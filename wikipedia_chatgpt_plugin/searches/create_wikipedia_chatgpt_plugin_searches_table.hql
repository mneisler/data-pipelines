--
-- Creates a table for stats about Wikipedia ChatGPT Plug-in usage of Wikipedia Search API, partitioned daily.
--
-- Parameters:
--     table_name      Fully qualified name of the table to create.
--     base_directory  HDFS path to use as the table's base location.
--
-- Usage:
--     spark3-sql -f create_wikipedia_chatgpt_plugin_searches_table.hql \
--         -d table_name=wmf_product.wikipedia_chatgpt_plugin_searches \
--         -d base_directory=/user/analytics-product/data/wikipedia_chatgpt_plugin_searches/daily
--


CREATE EXTERNAL TABLE IF NOT EXISTS ${table_name} (
    `wiki_db`        string  COMMENT 'MediaWiki database code',
    `search_count`   bigint  COMMENT 'Number of Cirrus Search requests made by Wikipedia ChatGPT plug-in'

)
PARTITIONED BY (
    `year`           int     COMMENT 'Unpadded year of request',
    `month`          int     COMMENT 'Unpadded month of request',
    `day`            int     COMMENT 'Unpadded day of request'
)
STORED AS PARQUET
LOCATION '${base_directory}'
;
