--
-- Creates a table for wikipedia preview stats, partitioned daily.
--
-- Parameters:
--     table_name      Fully qualified name of the table to create.
--     base_directory  HDFS path to use as the table's base location.
--
-- Usage:
--     spark3-sql -f create_wikipedia_preview_stats_table.hql \
--         -d table_name=wmf_product.wikipedia_preview_stats \
--         -d base_directory=/user/analytics-product/data/wikipedia_preview_stats/daily
--


CREATE EXTERNAL TABLE IF NOT EXISTS ${table_name} (
    `pageviews`      bigint  COMMENT 'Number of pageviews shown as a result of a clickthrough from a Wikipedia Preview preview',
    `previews`       bigint  COMMENT 'Number of API requests for article preview content made by Wikipedia Preview clients',
    `device_type`    string  COMMENT 'Type of device used by the client: touch or non-touch',
    `referer_host`   string  COMMENT 'Host from referer parsing',
    `continent`      string  COMMENT 'Continent of the accessing agents (maxmind GeoIP database)',
    `country_code`   string  COMMENT 'Country ISO code of the accessing agents (maxmind GeoIP database)',
    `country`        string  COMMENT 'Country (text) of the accessing agents (maxmind GeoIP database)',
    `instrumentation_version` int COMMENT 'Version number incremented along with major instrumentation changes'
)
PARTITIONED BY (
    `year`           int     COMMENT 'Unpadded year of request',
    `month`          int     COMMENT 'Unpadded month of request',
    `day`            int     COMMENT 'Unpadded day of request'
)
STORED AS PARQUET
LOCATION '${base_directory}'
;
